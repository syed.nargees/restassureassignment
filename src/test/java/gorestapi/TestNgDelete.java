package gorestapi;

import org.testng.annotations.Test;
import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.*;

public class TestNgDelete {
	@Test
    public void f() {
        
        baseURI = "https://gorest.co.in/public/v2";
        given().log().all().contentType("application/json")
                .header("authorization", "Bearer 7ab49ac21928c10cd5f2ef87b509b6337a2edafb0098424af8bee885f2a3920a")
                .delete("/users/193574").then().statusCode(204);
    }
}
