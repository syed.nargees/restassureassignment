package gorestapi;

import static io.restassured.RestAssured.baseURI;
import static io.restassured.RestAssured.given;

import org.testng.Assert;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.response.Response;

public class TestNGGetrequest {
	@Test
    private void assertGetMethod()
    {        
        //Response res = RestAssured.get("https://gorest.co.in/public/v2/users/193574");
        //int statusCode = res.getStatusCode();
       // Assert.assertEquals(200, statusCode);   
        baseURI = "https://gorest.co.in/public/v2";
		  given().log().all().contentType("application/json").
				 header("authorization","Bearer 7ab49ac21928c10cd5f2ef87b509b6337a2edafb0098424af8bee885f2a3920a")
				 .when().get("/users/193574").then().statusCode(200);
    }
}
